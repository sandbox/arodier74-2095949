This is a very simple module that creates a "SimpleDate" field.

The field stores only a date (no time). However, users can enter a full date (day, month & year) or just a year.
Exemple : '17/11/1987' (european), '11/17/1987' (US) or just '1987'.

This is especially useful for dates of birth, when some site's users do not want to enter their full date of birth.

The field is displayed using a simple text box, no frame is used.
